//Select element function
const selectElement = function (element) {
    return document.querySelector(element);
};
//Function For menu toggle
//Getting the menu icon
let menuToggler = selectElement('.menu-toggle');
//getting the body element
let body = selectElement('body');

//Adding a listener to the menu icon
menuToggler.addEventListener('click', function () {
    //Adding class 'open' to the body when the menu is clicked
    body.classList.toggle('open');
});

//Scroll reveal

window.sr = new ScrollReveal();

sr.reveal('.animate-left', {
    origin: 'left',
    duration: 1000,
    distance: '25rem',
    delay: 10
});

sr.reveal('.animate-right', {
    origin: 'right',
    duration: 1000,
    distance: '25rem',
    delay: 20
});

sr.reveal('.animate-top', {
    origin: 'top',
    duration: 800,
    distance: '25rem',
    delay: 600
});

sr.reveal('.animate-bottom', {
    origin: 'bottom',
    duration: 800,
    distance: '25rem',
    delay: 600
});